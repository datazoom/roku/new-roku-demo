Sub init()
    m.device = CreateObject("roDeviceInfo")
    m.global.observeField("input", "onInputReceived")
    m.eventConfig = invalid
    m.bufferFillVal = 0
    m.template = {}
    m.template.event = {}
    m.template.user_details = {}
    m.template.device = {}
    m.template.player ={}
    m.template.video ={}
    m.template.geo_location = {}
    m.template.network = {}
    m.template.custom = {}
    m.template.event.attributes = {}
    m.oldposition=0
    m.playerPlay = false
    m.playerPlayRequest = false
    m.playerResume = false
    m.bufferStarted = false
    m.videoPaused = false
    m.playStateTimer = m.top.findNode("playStateTimer")
    m.playStateTimer.ObserveField("fire","playStateTimerFunction")
    m.quartileTimer = m.top.findNode("quartileTimer")
    m.quartileTimer.ObserveField("fire","quartileTimerFunction")
    m.contentFirstQuartile = true
    m.contentSecondQuartile = true
    m.contentThirdQuartile = true
    m.quartilePresence = false
    m.dataTimer = m.top.findNode("dataTimer")
    m.dataTimer.ObserveField("fire","dataTimerFunction")
    m.renditionName = ""
    m.renditionBitrate = 0
    m.renditionVideoBitrate = 0
    m.renditionAudioBitrate = 0
    m.streamBitrate = 0
    m.mute = false
    m.defaultMute = false
    m.playCounter = 0
    m.defaultRate = 1
    m.milestonePercent = 0
    m.playerHeight = 0
    m.playerWidth = 0
    m.renditionHeight = 0
    m.renditionWidth = 0
    m.streamBitrate = 0
    m.systemBitrate = 0
    m.pausePoint = 0
'    m.sessionStartTimestamp = 0
    m.numberOfErrors = 0
    
    m.TSBUFFERSTART = 0
    m.TSSTALLSTART = 0
    m.TSLASTHEARTBEAT = 0
    m.TSLASTMILESTONE = 0
    m.TSPAUSED = 0
    m.TSREQUESTED = 0
    m.TSSTARTED = 0
    m.TSRENDITIONCHANGE = 0
    m.playerAction = ""
    
    m.fired10 = false
    m.fired25 = false
    m.fired50 = false
    m.fired75 = false
    m.fired90 = false
    m.fired95 = false
    
End Sub

'Function to get player Type
function playerType()
    return "native"
end function

'Function to get deviceType
function deviceType()
    return "ott device"
end function

'Function containing Datapoint List
Function getLibConfig()
    if m.config = invalid
        m.config = {
          events: {
            EVENTPLAY: "Play",
            EVENTPAUSE: "Pause",
            EVENTBUFFERING: "Buffering",
            EVENTBUFFEREND: "Buffer_End",
            EVENTBUFFERSTART: "Buffer_Start",
            EVENTSTALLSTART: "Stall_Start",
            EVENTSTALLEND: "Stall_End",
            EVENTFIRSTFRAME: "Playback_Start",
            EVENTPLAYBACKCOMPLETE: "Playback_Complete",
            EVENTPLAYREQUEST: "Play_Request",
            EVENTPLAYING: "Playing",
            EVENTRESUME: "Resume",
            EVENTPLAYERREADY: "Player_Ready",
            EVENTCFQ: "Content_First_Quartile",
            EVENTCSQ: "Content_Second_Quartile",
            EVENTCTQ: "Content_Third_Quartile",
            EVENTMUTE: "Mute",
            EVENTUNMUTE: "Unmute",
            EVENTBITRATECHANGE: "Bitrate_Change",
            EVENTADREQUEST: "Ad_Request",
            EVENTADCOMPLETE: "Ad_Complete",
            EVENTADIMPRESSION: "Ad_Impression",
            EVENTDATAZOOMLOADED: "Datazoom_Loaded",
            EVENTCUSTOM: "Custom_Event",
            EVENTHEARTBEAT: "Heartbeat",
            EVENTCONTENTLOADED: "Content_Loaded",
            EVENTMILESTONE: "Milestone",
            EVENTRENDITIONCHANGE: "Rendition_Change",
            EVENTSEEKSTART: "Seek_Start",
            EVENTSEEKEND: "Seek_End",
            EVENTERROR: "Error",
            
            METADURATION: "duration",
            METAIP: "client_ip",
            METACITY: "city",
            METACOUNTRYCODE: "countryCode",
            METAREGIONCODE: "regionCode",
            METAOS: "os",
            METADEVICETYPE: "deviceType",
            METADEVICEID: "device_id",
            METADEVICENAME: "deviceName",
            METADEVICEMFG: "deviceMfg",
            METAOSVERSION: "osVersion",
            METAASN: "asn",
            METAASNORG: "asnOrganization",
            METAISP: "isp",
            METACOUNTRY: "country",
            METAREGION: "region",
            METAZIP: "zipCode",
            
            METACONTROLS: "controls",
            METALOOP: "loop",
            METAREADYSTATE: "readyState",
            METASESSIONVIEWID: "SessionViewId",
            METAVIEWID: "viewId",
            METALONGITUDE: "longitude",
            METALATITUDE: "latitude",
            METADESCRIPTION: "description",
            METATITLE: "title",
            METAVIDEOTYPE: "videoType",
            METADEFAULTMUTED: "defaultMuted",
            METAISMUTED: "isMuted",
            METASOURCE: "source",
            METACUSTOM: "customMetadata",
            METADEFAULTPLAYBACKRATE: "defaultPlaybackRate",
            METAMILESTONEPERCENT: "milestonePercent",
            METAABSSHIFT: "absShift",
            METASEEKENDPOINT: "seekEndPoint",
            METASEEKSTARTPOINT: "seekStartPoint",
            METASESSIONSTARTTIMESTAMP: "sessionStartTimestamp",
            METAPLAYERHEIGHT: "playerHeight",
            METAPLAYERWIDTH: "playerWidth",
            METAFRAMERATE: "frameRate",
            METAADVERTISINGID: "advertisingId",
            METAERRORCODE: "errorCode",
            METAERRORMSG: "errorMsg",
            
            FLUXPLAYHEADUPDATE: "playheadPosition",
            FLUXBUFFERFILL: "bufferFill",
            FLUXPLAYERSTATE: "playerState",
            FLUXNUMBEROFVIDEOS: "numberOfVideos",
            FLUXRENDITIONBITRATE: "renditionBitrate",
            FLUXTSBUFFERSTART: "timeSinceBufferStart",
            FLUXTSSTALLSTART: "timeSinceStallStart",
            FLUXTSLASTHEARTBEAT: "timeSinceLastHeartbeat",
            FLUXTSLASTMILESTONE: "timeSinceLastMilestone",
            FLUXTSPAUSED: "timeSincePaused",
            FLUXTSREQUESTED: "timeSinceRequested",
            FLUXTSSTARTED: "timeSinceStarted",
            FLUXPLAYBACKDURATION: "playbackDuration",
            FLUXNUMBEROFADS: "numberOfAds",
            FLUXBITRATE: "bitrate",
            FLUXVIEWSTARTTIME: "viewStartTimestamp",
            FLUXRENDITIONHEIGHT: "renditionHeight",
            FLUXRENDITIONWIDTH: "renditionWidth",
            FLUXRENDITIONNAME: "renditionName",
            FLUXRENDITIONVIDEOBITRATE: "renditionVideoBitrate",
            FLUXRENDITIONAUDIOBITRATE: "renditionAudioBitrate",
            FLUXTSLASTRENDITIONCHANGE: "timeSinceLastRenditionChange",
            FLUXNUMBEROFERRORS: "numberOfErrors"
          }
        }
    end if
    return m.config
end Function


' Configure maximum possible events needed to be collected from player.
Sub configureEvents() 
    m.player = m.top.playerInit.player
    playStateTimerFunction()
    m.playStateTimer.control = "start"
    m.player.observeField("position", "OnHeadPositionChange")
    m.player.observeField("bufferingStatus","OnBufferingStatusChange")
    m.player.observeField("state","OnVideoPlayerStateChange")
    m.player.observeField("timedMetaData", "timedMetaDataChanged")
    m.defaultMute = m.player.mute
    m.quartileTimer.control = "start"
    m.dataTimer.control = "start"
    m.defaultRate = 1
    m.numberOfErrors = 0
    m.displayMode = m.device.getDisplayMode()
    m.displaySize = m.device.getDisplaySize()
    m.playerHeight = m.displaySize.h
    m.playerWidth = m.displaySize.w
End Sub

Sub timedMetaDataChanged()
print "Position" m.player.Position
print "timedMetaData" m.player.timedMetaData
end Sub

' Checks if specific data points is available or not
function dataPointValidation()
    ? "DATA POINT VALIDATION!!!!"
    If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
        print "EVENT MILESTONE TRUE QUARTILE PRESENCE"
        m.quartilePresence = true
    End If
    If checkIfEventConfigured(m.eventConfig.EVENTCSQ)
        m.quartilePresence = true
    End If
    If checkIfEventConfigured(m.eventConfig.EVENTCTQ)
        m.quartilePresence = true
    End If
end function

Function onInputReceived()
print "onInputReceived" 
End Function


' Player callback method on each seconds. Use to get the user current position in the playback. 
Sub OnHeadPositionChange()
    if m.quartilePresence
        resetQuartileTimerFunction()
    end if
    If m.top.events <> invalid
        if m.top.events.flux_data <> invalid
            fluxAvail = false
           For Each fluxType in m.top.events.flux_data
            fluxAvail=true
           end for
           ' print fluxCount
           if fluxAvail
                if((m.player.position-m.oldposition)<0)
                    m.oldposition=0
                end if
                if ((m.player.position-m.oldposition)*1000) >= m.top.events.interval
                    m.oldposition=m.player.position
                        fluxMetricsData()
                        
                        wsSend(getMessageTemplate("Heartbeat"))
                        ? "HEARTBEAT SENT"
                        m.TSLASTHEARTBEAT = getCurrentTimestampInMillis()
                end if
            end if
        end if
    end if
    'Calls base to set session time
    callSetSessionTime()
End  Sub

function fluxMetricsData()
    m.template.event.metrics = {}
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYHEADUPDATE)
        playHead = 0
        if m.player <> invalid
            if m.player.position <> invalid
                playHead = m.player.position
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXPLAYHEADUPDATE] = playHead
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYBACKDURATION)
        playbackDuration = 0
        if m.player <> invalid
            if m.player.position <> invalid
                playbackDuration = m.player.position
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXPLAYBACKDURATION] = playbackDuration
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXBUFFERFILL)
        m.template.event.metrics[m.eventConfig.FLUXBUFFERFILL] = m.bufferFillVal
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXPLAYERSTATE)
        playerState = ""
        if m.player <> invalid
            if m.player.state <> invalid
                playerState = m.player.state
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXPLAYERSTATE] = playerState
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFVIDEOS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFVIDEOS] = Val(getNoOfVideos())
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFERRORS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFERRORS] = m.numberOfErrors
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXNUMBEROFADS)
        m.template.event.metrics[m.eventConfig.FLUXNUMBEROFADS] = 1
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONBITRATE)
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONBITRATE] = m.renditionBitrate
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONVIDEOBITRATE)
        if m.player <> invalid
            if m.player.streamingSegment <> invalid
                if m.player.streamingSegment.segType = 2
                m.renditionVideoBitrate = m.player.streamingSegment.segBitrateBps
                end if
            else
            m.renditionVideoBitrate = m.renditionBitrate
           end if
            
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONVIDEOBITRATE] = m.renditionVideoBitrate
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONAUDIOBITRATE)
     if m.player <> invalid
            if m.player.streamingSegment <> invalid
                if m.player.streamingSegment.segType = 1
                m.renditionAudioBitrate = m.player.streamingSegment.segBitrateBps
                end if
                else
                m.renditionAudioBitrate = m.streamBitrate
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONAUDIOBITRATE] = m.renditionAudioBitrate
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONNAME)
        if m.player <> invalid
            if m.player.streamingSegment <> invalid
                m.renditionName = m.player.streamingSegment.segUrl
            end if
        end if
         m.template.event.metrics[m.eventConfig.FLUXRENDITIONNAME] = m.renditionName
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONWIDTH)
        if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.width > 0
                m.renditionWidth = m.player.downloadedSegment.width
                else
                m.renditionWidth = m.playerWidth
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONWIDTH] = m.renditionWidth
    end if
     If checkIfFluxConfigured(m.eventConfig.FLUXRENDITIONHEIGHT)
     if m.player <> invalid
            if m.player.downloadedSegment <> invalid
                if m.player.downloadedSegment.height > 0
                m.renditionHeight = m.player.downloadedSegment.height
                else
                m.renditionHeight = m.playerHeight
                end if
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXRENDITIONHEIGHT] = m.renditionHeight
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXBITRATE)
    if m.player <> invalid
            if m.player.state <> invalid
                bitrate = m.renditionBitrate
            end if
        end if
        m.template.event.metrics[m.eventConfig.FLUXBITRATE] = m.renditionBitrate
    end if

    ' Time Since flux metrics    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSBUFFERSTART)
        if m.TSBUFFERSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSBUFFERSTART] = m.TSBUFFERSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSBUFFERSTART] = getCurrentTimestampInMillis() - m.TSBUFFERSTART
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSTALLSTART)
        if m.TSSTALLSTART = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSTALLSTART] = m.TSSTALLSTART
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSTALLSTART] = getCurrentTimestampInMillis() - m.TSSTALLSTART
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTHEARTBEAT)
        if m.TSLASTHEARTBEAT = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = m.TSLASTHEARTBEAT
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTHEARTBEAT] = getCurrentTimestampInMillis() - m.TSLASTHEARTBEAT
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTMILESTONE)
        if m.TSLASTMILESTONE = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTMILESTONE] = m.TSLASTMILESTONE
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTMILESTONE] = getCurrentTimestampInMillis() - m.TSLASTMILESTONE
        end if
    end if
    If checkIfFluxConfigured(m.eventConfig.FLUXTSPAUSED)
        if m.TSPAUSED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSPAUSED] = m.TSPAUSED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSPAUSED] = getCurrentTimestampInMillis() - m.TSPAUSED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSREQUESTED)
        if m.TSREQUESTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSREQUESTED] = m.TSREQUESTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSREQUESTED] = getCurrentTimestampInMillis() - m.TSREQUESTED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXVIEWSTARTTIME)
        if m.TSREQUESTED = 0
            m.template.event.metrics[m.eventConfig.FLUXVIEWSTARTTIME] = m.TSREQUESTED
        else
            m.template.event.metrics[m.eventConfig.FLUXVIEWSTARTTIME] = getCurrentTimestampInMillis() - m.TSREQUESTED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSSTARTED)
        if m.TSSTARTED = 0
            m.template.event.metrics[m.eventConfig.FLUXTSSTARTED] = m.TSSTARTED
        else
            m.template.event.metrics[m.eventConfig.FLUXTSSTARTED] = getCurrentTimestampInMillis() - m.TSSTARTED
        end if
    end if
    
    If checkIfFluxConfigured(m.eventConfig.FLUXTSLASTRENDITIONCHANGE)
        if m.TSRENDITIONCHANGE = 0
            m.template.event.metrics[m.eventConfig.FLUXTSLASTRENDITIONCHANGE] = m.TSRENDITIONCHANGE
        else
            m.template.event.metrics[m.eventConfig.FLUXTSLASTRENDITIONCHANGE] = getCurrentTimestampInMillis() - m.TSRENDITIONCHANGE
        end if
    end if
    
end function

' Sets playheadPosition
function playerMetrics()
    m.template.event.metrics = {}
    'Change this variable value to set whether fluxdata values should be sent with events or not
    callFlexMetrics = true
    if callFlexMetrics
        fluxMetricsData()
    else
        playHead = 0
        If checkIfFluxConfigured(m.eventConfig.FLUXPLAYHEADUPDATE)
            if m.player <> invalid
                if m.player.position <> invalid
                   playHead = m.player.position
                end if
            end if
        end If
        m.template.event.metrics["playheadPosition"] = playHead
    end if
end function

' Player callback method for buffer status changes.
Sub OnBufferingStatusChange()
    if m.player.bufferingStatus<>invalid
        m.bufferFillVal = m.player.bufferingStatus.percentage
        ? "BUFFER PERCENT VALUE"; m.bufferFillVal, "Buffering Status: "; m.bufferStarted
        if(m.bufferFillVal >= 95)
        if m.bufferStarted
            m.bufferStarted = false
            playerMetrics()
            If checkIfEventConfigured(m.eventConfig.EVENTBUFFEREND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFEREND))
            End If
            If checkIfEventConfigured(m.eventConfig.EVENTSTALLEND)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLEND))
            End If
        end if
        end if
   end if
End Sub

' Function to set resume flag
function setResume()
    m.playerResume = true
end function

' Player state change callback method.
sub OnVideoPlayerStateChange()
    playerMetrics()
    print "VIDEO PLAYER STATE:"; m.player.state
    print "VIDEO PLAYER ACTION:"; m.playerAction.ToStr()
    print "VIDEO PLAYER CONTROL:"; m.player.control
    
    if m.player.state = "playing"
        m.resumePoint = m.player.position
        m.playCounter = m.playCounter + 1
        if m.playCounter >1
            setResume()
        end if
        m.videoPaused = false
        if m.playerResume
            m.resumePoint = m.player.position
            if m.pausePoint <> m.resumePoint
         If checkIfEventConfigured(m.eventConfig.EVENTSEEKEND)
                ? "PAUSE POINT:"; m.pausePoint
                m.template.event.attributes[m.eventConfig.METASEEKSTARTPOINT] = m.pausePoint * 1000
                m.template.event.attributes[m.eventConfig.METASEEKENDPOINT] = m.player.position * 1000
                wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKEND))
                m.template.event.attributes = {}
            End If   
        
        print ("SEEK EVENT")
        print "PAUSE TIME"; m.pausePoint; "STARTED:"; m.resumePoint;
        end if
'        if m.pausePoint > m.resumePoint 
'         If checkIfEventConfigured(m.eventConfig.EVENTSEEKEND)
'                m.template.event.attributes[m.eventConfig.METASEEKSTART] = m.pausePoint
'                m.template.event.attributes[m.eventConfig.METASEEKSTART] = m.startPoint
'                wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKEND))
'                m.template.event.attributes = {}
'            End If
'        print ("SEEK EVENT REW")
'        print "PAUSE TIME"; m.pausePoint; "STARTED:"; m.resumePoint;
'        end if
            If checkIfEventConfigured(m.eventConfig.EVENTRESUME)
                wsSend(getMessageTemplate(m.eventConfig.EVENTRESUME))
            End If 
        else
            setResume()
            If checkIfEventConfigured(m.eventConfig.EVENTPLAY)
                wsSend(getMessageTemplate(m.eventConfig.EVENTPLAY))
            End If
            If checkIfEventConfigured(m.eventConfig.EVENTFIRSTFRAME)
                wsSend(getMessageTemplate(m.eventConfig.EVENTFIRSTFRAME))
            End If
            If checkIfEventConfigured(m.eventConfig.EVENTADCOMPLETE)
                wsSend(getMessageTemplate(m.eventConfig.EVENTADCOMPLETE))
            End If
            
            m.TSSTARTED = getCurrentTimestampInMillis()
        end if
        If checkIfEventConfigured(m.eventConfig.EVENTPLAYING)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYING))
        End If
        
     else if m.player.state = "paused"
        m.videoPaused = true
        m.pausePoint = m.player.position
        m.TSPAUSED = getCurrentTimestampInMillis()
        If checkIfEventConfigured(m.eventConfig.EVENTPAUSE)
            wsSend(getMessageTemplate(m.eventConfig.EVENTPAUSE))
        End If
        if m.pausePoint <> m.resumePoint
        If checkIfEventConfigured(m.eventConfig.EVENTSEEKSTART)
                wsSend(getMessageTemplate(m.eventConfig.EVENTSEEKSTART))
            End If   
        print ("SEEK START")
        print "PAUSE TIME: "; m.pausePoint; "STARTED: "; m.resumePoint;
        end if
'        if m.pausePoint > m.resumePoint 
'        print ("SEEK EVENT REW")
'        print "PAUSE TIME"; m.pausePoint; "STARTED:"; m.resumePoint;
'        end if
     else if m.player.state = "buffering"
        m.bufferStarted = true
        m.TSBUFFERSTART = getCurrentTimestampInMillis()
        if m.player.position > 1
        m.TSSTALLSTART = getCurrentTimestampInMillis()
        end if
        If checkIfEventConfigured(m.eventConfig.EVENTBUFFERING)
          wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERING))
        End If
        If checkIfEventConfigured(m.eventConfig.EVENTBUFFERSTART)
          wsSend(getMessageTemplate(m.eventConfig.EVENTBUFFERSTART))
        End If
        If checkIfEventConfigured(m.eventConfig.EVENTSTALLSTART)
          wsSend(getMessageTemplate(m.eventConfig.EVENTSTALLSTART))
        End If
     else if m.player.state = "error"
        If checkIfEventConfigured(m.eventConfig.EVENTERROR)
          m.template.event.attributes[m.eventConfig.METAERRORCODE] = m.player.errorCode.ToStr()
          m.template.event.attributes[m.eventConfig.METAERRORMSG] = m.player.errorMsg
          wsSend(getMessageTemplate(m.eventConfig.EVENTERROR)) 
          m.numberOfErrors = m.numberOfErrors + 1   
          m.template.event.attributes = {}  
        end if   
        
    else if m.player.state = "finished"
    If checkIfEventConfigured(m.eventConfig.EVENTPLAYBACKCOMPLETE)
         wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYBACKCOMPLETE))
    End If
  
    end if    
end Sub

' Function to reset quartile flags
sub resetQuartileTimerFunction()
    if m.player <> invalid
        if m.player.position <> invalid and m.player.duration <> invalid
            playerPosition = m.player.position
            playerDuration = m.player.duration
            if playerDuration > 0 and playerPosition > 0
                if playerPosition < (playerDuration * 0.25)
                    m.contentThirdQuartile = true
                    m.contentSecondQuartile = true
                    m.contentFirstQuartile = true
                else if playerPosition < (playerDuration * 0.50)
                    m.contentThirdQuartile = true
                    m.contentSecondQuartile = true
                else if playerPosition < (playerDuration * 0.75)
                    m.contentThirdQuartile = true
                end if
            end if
        end if
    end if
end sub

' Runs at specific interval to check is quartile position is reached
sub quartileTimerFunction()
    videoRunning = false
    if m.player <> invalid
        if m.player.position <> invalid
             ? "POSITIONS NEW/OLD"; m.player.position; m.oldposition
            if m.player.position <> m.oldposition
                videoRunning = true
            end if
        end if
    end if
    ? "quartilePresence / VideoRunning"; m.quartilePresence; videoRunning
    if videoRunning
        if m.player <> invalid
            if m.player.position <> invalid and m.player.duration <> invalid
            
                playerPosition = m.player.position
                playerDuration = m.player.duration
                ? "MILESTONE DURATION "; playerDuration
                ? "MILESTONE POSITION"; playerPosition
                m.template.event.attributes = {}
'                if playerDuration = 0 and playerPosition = 0
'                If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
'                                If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
'                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.10
'                                end if
'                                m.milestonePercent = 0.10
'                                playerMetrics()
'                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
'                                m.template.event.attributes = {}
'                            End If
'                End If
                if playerDuration > 0 and playerPosition > 0
                    ? "PLAYER IS RUNNING TO MILESTONE"
'                    if playerPosition <= playerDuration - 1
                        if playerPosition >= (playerDuration * 0.95) and m.fired95 = false
                            
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                                If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.95
                                end if
                                m.milestonePercent = 0.95
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired95 = true
                                m.template.event.attributes = {}
                            End If
'                        end if
                        else if playerPosition >= (playerDuration * 0.9) and m.fired90 = false
                        if playerPosition < ((playerDuration * 0.9)+2) and m.fired90 = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.9
                                end if
                                m.milestonePercent = 0.9
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired90 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                    else if playerPosition >= (playerDuration * 0.75)  and m.fired75 = false
                        if playerPosition < ((playerDuration * 0.75)+1) and m.fired75 = false' and m.contentSecondQuartile
                            'm.contentSecondQuartile = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.75
                                end if
                                m.milestonePercent = 0.75
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired75 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                    else if playerPosition >= (playerDuration * 0.50) and m.fired50 = false
                        if playerPosition < ((playerDuration * 0.50)+1) and m.fired50 = false' and m.contentSecondQuartile
                            'm.contentSecondQuartile = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.50
                                end if
                                m.milestonePercent = 0.50
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired50 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                        else if playerPosition >= (playerDuration * 0.25) and m.fired25 = false
                        if playerPosition < ((playerDuration * 0.25)+1) and m.fired25 = false' and m.contentSecondQuartile
                            'm.contentSecondQuartile = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.25
                            end if
                                m.milestonePercent = 0.25
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired25 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                    else if playerPosition >= (playerDuration * 0.11) and m.fired10 = false
                         ? "MILESTONE PASSED"
                        if playerPosition <= ((playerDuration * 0.11)+1) and m.fired10 = false' and m.contentFirstQuartile
                        ? "MILESTONE LIMIT"
                            'm.contentFirstQuartile = false
                            If checkIfEventConfigured(m.eventConfig.EVENTMILESTONE)
                            ? "MILESTONE CONFIGURED"
                            If checkIfMetaConfigured(m.eventConfig.METAMILESTONEPERCENT)
                                m.template.event.attributes[m.eventConfig.METAMILESTONEPERCENT] = 0.10
                            end if
                                m.milestonePercent = 0.10
                                playerMetrics()
                                wsSend(getMessageTemplate(m.eventConfig.EVENTMILESTONE))
                                m.TSLASTMILESTONE = getCurrentTimestampInMillis()
                                m.fired10 = true
                                m.template.event.attributes = {}
                            End If
                        end if
                    end if
                end if
            end if
        end if
    end if
end sub

' Runs at specific interval to update the data
sub dataTimerFunction()
    videoRunning = false
    if m.player <> invalid
        'is video running?
        if m.player.position <> invalid
            if m.player.position <> m.oldposition
                videoRunning = true
            end if
        end if
        'is video Muted
        if videoRunning
           videoMute= m.player.mute
           if m.mute <> videoMute
            m.mute = videoMute
            if m.mute
                If checkIfEventConfigured(m.eventConfig.EVENTMUTE)
                  playerMetrics()
                  wsSend(getMessageTemplate(m.eventConfig.EVENTMUTE))
                End If
            else
                If checkIfEventConfigured(m.eventConfig.EVENTUNMUTE)
                  playerMetrics()
                  wsSend(getMessageTemplate(m.eventConfig.EVENTUNMUTE))
                End If
            end if
        end if
        
       'Collect stream metadata and info
       
       'timedMetadata
       m.player.timedMetaDataSelectionKeys = ["*"]
       if m.player.timedMetaData <> invalid
        ? "+++++    TIMED METADATA   +++++"
        ? m.player.timedMetaData
            ba = CreateObject("roByteArray")
            ba.FromHExString(m.player.timedMetaData.PRIV)
            strg = ba.ToAsciiString()
         ? "+++++++++++++++++++++++++++++++"
        end if
       
       'stream info
       if m.player.streamInfo <> invalid 
'           print "STREAMINFO:"; m.player.streamInfo
'           print "SYSTEMBITRATE:"; m.player.streamInfo.measuredBitrate
       end if 
       
       ' downloaded segment
       if m.player.downloadedSegment <> invalid
'           print "DOWNLOADED SEGMENT:"; m.player.downloadedSegment
'           ? "---------------------------------------------------------------------"
'           print "SEGMENT HEIGHT:"; m.player.downloadedSegment.height
'           print "SEGMENT WIDTH:"; m.player.downloadedSegment.width
'           print "DOWNLOADED BITRATE"; m.player.downloadedSegment.bitrateBPS
       end if
       
       'streaming segment
       if m.player.streamingSegment <> invalid 
           if m.player.streamingSegment.segBitrateBps <> invalid or m.player.streamingSegment.segBitrateBps <> 0 
           m.renditionBitrate = m.player.streamingSegment.segBitrateBps
           else if m.player.streamBitrate <> invalid or m.player.streamBitrate <> 0 
           m.renditionBitrate = m.player.streamBitrate
           else if m.player.streamInfo.streamBitrate <> invalid or m.player.streamInfo.streamBitrate <> 0 
           m.streamBitrate = m.player.streamInfo.streamBitrate
       end if
'           print "STREAMING SEGMENT:"; m.player.streamingSegment
'           print "STREAMBITRATE:"; m.player.streamingSegment.segBitrateBps
'           print "RENDITION HEIGHT:"; m.player.streamingSegment.height
'           print "RENDITION WIDTH:"; m.player.streamingSegment.width
        end if

'                 complete player info
'                 print "PLAYER: "; m.player

'                 for each item in m.player.items()
'                 print item.key; " = "; item.value
'                 end for

'                 print "FOCUSEDCHILD"; m.player.focusedChild.maxVideoDecodeResolution
'                 print "BIFDISPLAY:"; m.player.bifDisplay
'                 print "PLAYSTARTINFO: "; m.player.playStartInfo
'                 print "MAXVIDEORESOLUTION: ";m.player.maxVideoDecodeResolution
'                 print "POSITION INFO:"; m.player.positionInfo
'                 print "TRACKS: "; m.player.tracks
'                 print "CLIPPINGRECT: "; m.player.clippingRect
'                 print "PLAYER.SCALE:"; m.player.scale
'                 print "FORMAT"; m.player.videoFormat
'                 print "FRAME RATE: "; m.player.FrameRate
'                 
'                 if m.player.content <> invalid
'                 print "CONTENT: "; m.player.content
'                 print "CONTENT CHANGE: "; m.player.content.change
'                 print "CONTENT CATEGORIES: "; m.player.content.categories
'                 print "IS HD? :"; m.player.content.IsHD
'                 print "STREAMINFO:"; m.player.content.stream
'                 print "STREAMSINFO: "; m.player.content.Streams
'                 print "STREAMBITRATESS: "; m.player.content.StreamBitrates
'                 print "CONTENT:"; m.player.content.contentType
'                 end if
'                 

'                 
'                 if m.player.downloadedSegment <> invalid
'                 print "DOWNLOADED SEGMENT:"; m.player.downloadedSegment
'                 print "SEGMENT HEIGHT:"; m.player.downloadedSegment.height
'                 print "SEGMENT WIDTH:"; m.player.downloadedSegment.width
'                 print "DOWNLOADED BITRATE"; m.player.downloadedSegment.bitrateBPS
'                 end if
'                 
'                 if m.player.streamInfo <> invalid 
'                 print "STREAMINFO:"; m.player.streamInfo
'                 print "SYSTEMBITRATE:"; m.player.streamInfo.measuredBitrate
'                 end if 
'                 
                 if m.player.manifestData <> invalid
                 
'                 print "MANIFEST: "; m.player.manifestData
                 print "MANIFEST.MPD.ITEMS"
'                 for each item in m.player.manifestData.mpd.Items()
'                 print item.key, "=", item.value
'                 end for
                 end if
                  
        
        if m.player.streamingSegment <> invalid
            if m.player.streamingSegment.segBitrateBps <> invalid
                 streamB = m.player.streamingSegment.segBitrateBps
              
                 m.renditionBitrate  = streamB
                 
                 if m.streamBitrate <> streamB
                 m.streamBitrate = streamB
                 playerMetrics()
                   if streamB < m.streamBitrate
                   m.template.event.attributes[m.eventConfig.METAABSSHIFT] = "up"
                   end if
                   if streamB > m.streamBitrate
                   m.template.event.attributes[m.eventConfig.METAABSSHIFT] = "down"
                   end if
                 m.streamBitrate = streamB
                 If checkIfEventConfigured(m.eventConfig.EVENTBITRATECHANGE)
                 playerMetrics()
                 wsSend(getMessageTemplate(m.eventConfig.EVENTBITRATECHANGE))
                 end if
                 If checkIfEventConfigured(m.eventConfig.EVENTRENDITIONCHANGE)
                 playerMetrics()
                 wsSend(getMessageTemplate(m.eventConfig.EVENTRENDITIONCHANGE))
                 m.TSRENDITIONCHANGE = getCurrentTimestampInMillis()
                 m.template.event.attributes = {}
                 end if
                 end if
            end if
        end if
    end if
    end if
end sub

' Checks when player is ready to play
function playStateTimerFunction()

    if m.player <> invalid
        if m.player.control <> invalid
            if m.player.control <> m.playerAction
                if Lcase(m.player.control) = "none" and Lcase(m.playerAction) = ""
'                ? "player control:"; m.playercontrol, "playe action:"; m.player.action
'                ? "PLAYER STATE",Lcase(m.player.control), "ACTION", Lcase(m.playerAction)  
'                ? "EVENT CONFIG"; m.eventConfig{}  
'                ? "IS EVENT CONFIGURED", checkIfEventConfigured(m.eventConfig.EVENTADREQUEST)
'                ? "IS EVENT CONFIGURED", checkIfEventConfigured(m.eventConfig.EVENTADIMPRESSION)          
                    If checkIfEventConfigured(m.eventConfig.EVENTADREQUEST)
'                        ? "SEND AD REQUEST"
                        wsSend(getMessageTemplate(m.eventConfig.EVENTADREQUEST))
                    End If
                        If checkIfEventConfigured(m.eventConfig.EVENTADIMPRESSION)
'                        ? "SEND AD IMPRESSION"
                        wsSend(getMessageTemplate(m.eventConfig.EVENTADIMPRESSION))
                        End If
                end if
                m.playerAction = m.player.control
                if LCase(m.playerAction) = "play"
                    playerMetrics()
                    If checkIfEventConfigured(m.eventConfig.EVENTADREQUEST)
'                        ? "SEND AD REQUEST"
                        wsSend(getMessageTemplate(m.eventConfig.EVENTADREQUEST))
                    End If
                        If checkIfEventConfigured(m.eventConfig.EVENTADIMPRESSION)
'                        ? "SEND AD IMPRESSION"
                        wsSend(getMessageTemplate(m.eventConfig.EVENTADIMPRESSION))
                        End If
                    m.TSREQUESTED = getCurrentTimestampInMillis()
                    If checkIfEventConfigured(m.eventConfig.EVENTPLAYERREADY)
                     wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYERREADY))
                    End If
                    If checkIfEventConfigured(m.eventConfig.EVENTCONTENTLOADED)
                     wsSend(getMessageTemplate(m.eventConfig.EVENTCONTENTLOADED))
                    End If
                    If checkIfEventConfigured(m.eventConfig.EVENTPLAYREQUEST)
                        wsSend(getMessageTemplate(m.eventConfig.EVENTPLAYREQUEST))
                    End If
                    
                end if
            end if
        end if
    end if
end function


'----------------------- Collector Starts --------------------------------

' Method to get the message template
Function getMessageTemplate(event as String) as object
    If checkIfMetaConfigured(m.eventConfig.METASESSIONVIEWID)
         getContentUrl()
    end if
    
    If checkIfMetaConfigured(m.eventConfig.METAVIEWID)
         getContentUrl()
    end if
     m.template.customer_code = m.responseBody.customer_code
     m.template.connector_list= m.responseBody.connector_list
     m.template.configuration_id = m.responseBody.configuration_id
     m.template.event_id = m.device.GetRandomUUID()
     
     m.template.event.type = event
     m.template.event.timestamp = getCurrentTimestampInMillis()
     
     'm.template.device.id = getUniqueDeviceId()
     
     m.template.user_details.is_anonymous = false
     m.template.user_details.session_id = getSessionData()
'     m.template.user_details.sessionStartTimestamp = m.sessionIdTimeKey
'     print "SESSION START = "; m.sessionIdTimeKey
     m.template.player["playerName"] = "Roku"
     m.template.player["playerVersion"] = getVersion()
     getMetaData()
     
     return m.template
End Function

'Function to get the metadata
function getMetaData()
    if m.player <> invalid
    If checkIfMetaConfigured(m.eventConfig.METADURATION)
        playerDuration = 0
        if m.player <> invalid
            if m.player.duration <> invalid
                playerDuration = m.player.duration
            end if
        end if
        m.template.video[m.eventConfig.METADURATION] = playerDuration
    end if
     If checkIfMetaConfigured(m.eventConfig.METAIP)
        m.template.user_details[m.eventConfig.METAIP] = getIpAddress()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACITY)
        m.template.geo_location[m.eventConfig.METACITY] = getCity()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAZIP)
        m.template.geo_location[m.eventConfig.METAZIP] = getZip()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACOUNTRYCODE)
        m.template.geo_location[m.eventConfig.METACOUNTRYCODE] = getCountryCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACOUNTRY)
        m.template.geo_location[m.eventConfig.METACOUNTRY] = getCountry()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREGIONCODE)
        m.template.geo_location[m.eventConfig.METAREGIONCODE] = getRegionCode()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREGION)
        m.template.geo_location[m.eventConfig.METAREGION] = getRegion()
    end if
        If checkIfMetaConfigured(m.eventConfig.METAOS)
        m.template.device[m.eventConfig.METAOS] = getOS()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICETYPE)
        m.template.device[m.eventConfig.METADEVICETYPE] = getDeviceType()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICEID)
        m.template.device[m.eventConfig.METADEVICEID] = getUniqueDeviceId()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAADVERTISINGID)
        adId = getAdId()
        m.template.device[m.eventConfig.METAADVERTISINGID] = getAdId()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICENAME)
        m.template.device[m.eventConfig.METADEVICENAME] = getModelDisplayName()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEVICEMFG)
        m.template.device[m.eventConfig.METADEVICEMFG] = "Roku, Inc."
    end if
    If checkIfMetaConfigured(m.eventConfig.METAVIDEOTYPE)
        m.template.device[m.eventConfig.METAVIDEOTYPE] = getVideoType()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAOSVERSION)
        m.template.device[m.eventConfig.METAOSVERSION] = getOSVersion()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAASN)
        m.template.network[m.eventConfig.METAASN] = getasn()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAASNORG)
        m.template.network[m.eventConfig.METAASNORG] = getasnOrg()
    end if
    If checkIfMetaConfigured(m.eventConfig.METAISP)
        m.template.network[m.eventConfig.METAISP] = getISP()
    end if
    If checkIfMetaConfigured(m.eventConfig.METACONTROLS)
         metacontrol = false
        if m.player <> invalid
            if m.player.control <> invalid
                if m.player.control <> ""
                    metacontrol = true
                end if
            end if
        end if
        m.template.player[m.eventConfig.METACONTROLS] = metacontrol
    end if
    If checkIfMetaConfigured(m.eventConfig.METALOOP)
        metaloop = false
        if m.player <> invalid
            if m.player.loop <> invalid
                if m.player.loop
                    metaloop = true
                end if
            end if
        end if
        m.template.player[m.eventConfig.METALOOP] = metaloop
    end if
    If checkIfMetaConfigured(m.eventConfig.METAREADYSTATE)
        m.template.player[m.eventConfig.METAREADYSTATE] = getPlayerReadyState()
    end if
    If checkIfMetaConfigured(m.eventConfig.METASESSIONVIEWID)
        m.template.user_details[m.eventConfig.METASESSIONVIEWID] = getSessionViewId()
    end if
'    If checkIfMetaConfigured(m.eventConfig.METASESSIONSTARTTIMESTAMP)
''        print "BASE REGISTER = "; m.dzBaseRegister()
'        m.template.user_details[m.eventConfig.METASESSIONSTARTTIMESTAMP] = m.sessionStartTimestamp
'    end if
    If checkIfMetaConfigured(m.eventConfig.METAVIEWID)
        m.template.user_details[m.eventConfig.METAVIEWID] = getSessionViewId()
    end if
    
    If checkIfMetaConfigured(m.eventConfig.METALONGITUDE)
        m.template.geo_location[m.eventConfig.METALONGITUDE] = getLongitude()
    end if
    If checkIfMetaConfigured(m.eventConfig.METALATITUDE)
        m.template.geo_location[m.eventConfig.METALATITUDE] = getLatitude()
    end if
    If checkIfMetaConfigured(m.eventConfig.METADESCRIPTION)
        metadescription = ""
        if m.player <> invalid
            if m.player.content.description <> invalid
               metadescription = m.player.content.description
            end if
        end if
        m.template.video[m.eventConfig.METADESCRIPTION] = metadescription
    end if
    If checkIfMetaConfigured(m.eventConfig.METATITLE)
        metatitle = ""
        if m.player <> invalid
            if m.player.content.title <> invalid
               metatitle = m.player.content.title
            end if
            if m.player.content.url <> invalid
               metaurl = m.player.content.url
            end if
        end if
        m.template.video[m.eventConfig.METATITLE] = metatitle
    end if
        If checkIfMetaConfigured(m.eventConfig.METASOURCE)
        metatitle = ""
        if m.player <> invalid
            if m.player.content.url <> invalid
               metaurl = m.player.content.url
            end if
        end if
        m.template.video[m.eventConfig.METASOURCE] = metaurl
    end if
    If checkIfMetaConfigured(m.eventConfig.METAISMUTED)
        m.template.player[m.eventConfig.METAISMUTED] = m.mute
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEFAULTMUTED)
        m.template.player[m.eventConfig.METADEFAULTMUTED] = m.defaultMute
    end if
    If checkIfMetaConfigured(m.eventConfig.METADEFAULTPLAYBACKRATE)
        m.template.player[m.eventConfig.METADEFAULTPLAYBACKRATE] = m.defaultRate
    end if
        If checkIfMetaConfigured(m.eventConfig.METAPLAYERHEIGHT)
        m.template.player[m.eventConfig.METAPLAYERHEIGHT] = m.playerHeight
    end if
          If checkIfMetaConfigured(m.eventConfig.METAPLAYERWIDTH)
        m.template.player[m.eventConfig.METAPLAYERWIDTH] = m.playerWidth
    end if
    If checkIfMetaConfigured(m.eventConfig.METATITLE)
        metatitle = ""
        if m.player <> invalid
            if m.player.content.title <> invalid
               metatitle = m.player.content.title
            end if
            if m.player.content.url <> invalid
               metaurl = m.player.content.url
            end if
        end if
        m.template.video[m.eventConfig.METATITLE] = metatitle
    end if
    
    If checkIfMetaConfigured(m.eventConfig.METAFRAMERATE)
        if m.player.FrameRate <> invalid
        Print "PLAYER FRAMERATE"; m.player.framerate
        m.template.video[m.eventConfig.METAFRAMERATE] = m.player.FrameRate
        else 
        print "DEVICE FRAMERATE"; "30"
        m.template.video[m.eventConfig.METAFRAMERATE] = 30
        end if
    end if
        
    End if
        
end function

' Function to get video URL/set url in base
function getContentUrl()
    if m.player <> invalid
       if m.player.content <> invalid
            if m.player.content.Url <> invalid
                setContentUrlToBase(m.player.content.Url)
                return m.player.content.Url
            else
                return ""
            end if
        else
            return ""
        end if
    end if
end function