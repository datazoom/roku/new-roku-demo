'*********************************************************************
'** (c) 2016-2017 Roku, Inc.  All content herein is protected by U.S.
'** copyright and other applicable intellectual property laws and may
'** not be copied without the express permission of Roku, Inc., which
'** reserves all rights.  Reuse of any of this content for any purpose
'** without the permission of Roku, Inc. is strictly prohibited.
'*********************************************************************

sub init()   

       m.list = m.top.FindNode("list")
       m.list.observeField("itemSelected", "onItemSelected")
       m.list.SetFocus(true)
       
       'INIT DATAZOOM SDK
       m.DzLib = m.top.findNode("DzLib") 
       DzCollector()

    'descriptor for the menu items
    itemList = [
        {
            title: "VOD / MP4"
            url: "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4"'"https://demo.datazoomlabs.com/vod/example.mp4"' "https://commondatastorage.googleapis.com/gtv-videos-bucket/sample/BigBuckBunny.mp4" 
        }
        {
            title: "M3U8 / HLS)"
            url: "https://demo.datazoomlabs.com/vod/example.m3u8"
        }
        {
            title: "VOD / MPD"
            url:  "http://www.bok.net/dash/tears_of_steel/cleartext/stream.mpd"'"https://s3.amazonaws.com/_bc_dml/example-content/sintel_dash/sintel_vod.mpd"
        }
        {
            title: "Live Stream"
            url: "https://drive.google.com/file/d/16uk02dPLc_Kap50rKuVxYJX39USJn0u1/view?usp=sharing"' "https://liveprodeuwest.akamaized.net/eu1/Channel-EUTVqvs-AWS-ireland-1/Source-EUTVqvs-1000-1_live.m3u8"
        }
        {   title: "Send Custom Event Before Player Init"
        }
        {   title: "Select DataZoom Environment"
        }
    ]

    ' compile into a ContentNode structure
    listNode = CreateObject("roSGNode", "ContentNode")
    for each item in itemList:
        nod = CreateObject("roSGNode", "ContentNode")
        nod.setFields(item)
        listNode.appendChild(nod)
    next
    m.list.content = listNode
end sub

sub onItemSelected()
    m.list.SetFocus(false) ' un-set focus to avoid creating multiple players on user tapping twice
    menuItem = m.list.content.getChild(m.list.itemSelected)
    print "TITLE:" menuItem.title
    
   If menuItem.title = "Send Custom Event Before Player Init" then
   print ":" menuItem.title
   ? "Send Custom Event"
   sendCustomEventAndMeta()
   m.list.SetFocus(true)
   
   else if menuItem.title = "Select DataZoom Environment" then
   print ":" menuItem.title
   ? "SELECT ENV."
   
   newScreen()
'   onItemSelected()
   dzURL = menuItem.url
   
   else 
    videoContent = {
        streamFormat: menuItem.title,
        titleSeason: "DataZoomDemo - Roku ",
        title: menuItem.title,
        url:  menuItem.url,

        'used for raf.setContentGenre(). For ads provided by the Roku ad service, see docs on 'Roku Genre Tags'
        categories: ["Animated"]

        'Roku mandates that all channels enable Nielsen DAR
        nielsen_app_id: "P2871BBFF-1A28-44AA-AF68-C7DE4B148C32" 'required, put "P2871BBFF-1A28-44AA-AF68-C7DE4B148C32", Roku's default appId if not having ID from Nielsen
        nielsen_genre: "GV" 'required, put "GV" if dynamic genre or special circumstances (e.g. games)
        nielsen_program_id: "Big Buck Bunny" 'movie title or series name
        length: 3220 'in seconds;
    }
    ' compile into a VideoContent node
    content = CreateObject("roSGNode", "VideoContent")
    content.setFields(videoContent)
    content.ad_url = ""

    if m.Player = invalid:
        m.Player = m.top.CreateChild("Player")
        m.Player.observeField("state", "PlayerStateChanged")
    end if

    'start the player
    m.Player.content = content
    m.Player.visible = true
    m.Player.control = "play"
    end if
end sub

sub sendCustomEventAndMeta()
    m.DzLib.customEvent = "Custom_CUSTOM EVENT BEFORE PLAYER INIT"
    m.DzLib.customMeta = "Custom_CUSTOM META BEFORE PLAYER INIT"
    m.DzLib.sendCustomMeta = true
    m.DzLib.sendCustomEvent = true
    
End sub

Sub DzCollector()
        m.DzLib.libConfiguration = {
             'player : m.Player,
             configURL : "https://platform.datazoom.io",
             configId : "5439f24d-fbfd-4e24-a62b-40f2f8ec775c"      
        }
        m.DzLib.initiateCollector = true
End Sub



sub PlayerStateChanged()
    print "EntryScene: PlayerStateChanged(), state = "; m.Player.state
    if m.Player.state = "done" or m.Player.state = "stop"
        m.Player.visible = false
        m.list.setFocus(true) 'NB. the player took the focus away, so get it back
    end if
end sub

sub newScreen()
        m.list.SetFocus(true)

    itemList = [

        {
            title: "DEV URL"
            url: "https://devplatform.datazoom.io/beacon/v1/" 
        }
        {
            title: "STAGING URL)"
            url: "https://stagingplatform.datazoom.io/beacon/v1/"
        }
        {
            title: "PRODUCTION URL"
            url: "https://platform.datazoom.io/beacon/v1/"
        }
    
    ]

    ' compile into a ContentNode structure
    listNode = CreateObject("roSGNode", "ContentNode")
    for each item in itemList:
        nod = CreateObject("roSGNode", "ContentNode")
        nod.setFields(item)
        listNode.appendChild(nod)
    next
    m.list.content = listNode
    end sub

Function CreateLunchMenu() as integer
    screen = CreateObject("roGridScreen")
    port = CreateObject("roMessagePort")
    screen.SetMessagePort(port)
    screen.show()

    while (true)
      msg = wait(0, port)
      if type(msg) = "roGridScreenEvent"
        if (msg.isScreenClosed())
          return -1
        endif
      endif
    end while
End Function
