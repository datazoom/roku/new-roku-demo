'*********************************************************************
'** (c) 2016-2017 Roku, Inc.  All content herein is protected by U.S.
'** copyright and other applicable intellectual property laws and may
'** not be copied without the express permission of Roku, Inc., which
'** reserves all rights.  Reuse of any of this content for any purpose
'** without the permission of Roku, Inc. is strictly prohibited.
'*********************************************************************

sub init()   
       m.list = m.top.FindNode("list")
       m.list.observeField("itemSelected", "onItemSelected")
       m.list.SetFocus(true)
       envUrl = "https://devplatform.datazoom.io/beacon/v1/"

    'descriptor for the menu items
    itemList = [
        {
            title: "DEV URL"
            url: "https://devplatform.datazoom.io/beacon/v1/" 
        }
        {
            title: "STAGING URL)"
            url: "https://stagingplatform.datazoom.io/beacon/v1/"
        }
        {
            title: "PRODUCTION URL"
            url: "https://platform.datazoom.io/beacon/v1/"
        }
    ]

    ' compile into a ContentNode structure
    listNode = CreateObject("roSGNode", "ContentNode")
    for each item in itemList:
        nod = CreateObject("roSGNode", "ContentNode")
        nod.setFields(item)
        listNode.appendChild(nod)
    next
    m.list.content = listNode
end sub

sub onItemSelected()
'    m.list.SetFocus(false) ' un-set focus to avoid creating multiple players on user tapping twice
    menuItem = m.list.content.getChild(m.list.itemSelected)
   
   ? "SELECTED CONFIG ID:"
   ? menuitem.url 
   

end sub



