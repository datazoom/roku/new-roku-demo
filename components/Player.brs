'*********************************************************************
'** (c) 2016-2017 Roku, Inc.  All content herein is protected by U.S.
'** copyright and other applicable intellectual property laws and may
'** not be copied without the express permission of Roku, Inc., which
'** reserves all rights.  Reuse of any of this content for any purpose
'** without the permission of Roku, Inc. is strictly prohibited.
'********************************************************************* 

' Player

sub init()
    m.top.SetFocus(true)
    m.video = m.top.CreateChild("Video")    
    m.DzLib = m.top.findNode("DzLib") 
    DzCollector()  
    DzPlayer()      
end sub

Sub DzCollector()
        m.DzLib.libConfiguration = {
             player : m.video,
             configURL : "https://platform.datazoom.io",
             configId : "5439f24d-fbfd-4e24-a62b-40f2f8ec775c"        
        }
        m.DzLib.initiateCollector = true
End Sub

Sub DzPlayer()
        m.DzLib.playerInit = {
'        dzSessionViewId : m.DZ_SESSION_VIEW_ID,
'        dzSessionId : m.DZ_SESSION_ID,
        player: m.video
        }
        ? "SESSION ID:"
        ? "SESSION VIEW ID:"
        m.DzLib.initiatePlayer = true 
'        m.DzLib.initiateCollector = true
End Sub

sub sendCustomEventAndMeta()
    m.DzLib.customEvent = "Custom_CUSTOM EVENT ON BACK BUTTON"
    m.DzLib.customMeta = "Custom_CUSTOM META ON BACK BUTTON"
    m.DzLib.sendCustomEvent = true
    m.DzLib.sendCustomMeta = true
End sub

sub sendAdEvent()
    ? "SEND AD EVENT!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"
End sub

sub controlChanged()
    'handle orders by the parent/owner
    control = m.top.control
    if control = "play" then
        playContent()
    else if control = "stop" then
        exitPlayer()
    end if
end sub

sub playContent()
    content = m.top.content
    if content <> invalid then
        m.video.content = content
        m.video.visible = false
        m.PlayerTask = CreateObject("roSGNode", "PlayerTask")
        m.PlayerTask.observeField("state", "taskStateChanged")
        m.PlayerTask.observeField("milesPerc", "sendAdEvent")
        m.PlayerTask.video = m.video
'        m.DzLib = m.top.findNode("DzLib")
'        DzCollector()  
'        DzPlayer()      
        m.PlayerTask.control = "RUN"
    end if
end sub



sub exitPlayer()
    print "Player: exitPlayer()"
    m.video.control = "stop"
    m.video.visible = false
    m.PlayerTask = invalid
    m.player = invalid
    'signal upwards that we are done
    m.top.state = "done"
end sub

sub taskStateChanged(event as Object)
    print "Player: taskStateChanged(), id = "; event.getNode(); ", Field = "; event.getField(); " Data = "; event.getData()
    state = event.GetData()
    if state = "done" or state = "stop" or state = "error"
        exitPlayer()
    end if
end sub
sub adMilestoneChanged(event as Object)
    print "Player: taskStateChanged(), id = "; event.getNode(); ", Field = "; event.getField(); " Data = "; event.getData()
    state = event.GetData()
    if state = "done" or state = "stop"
        exitPlayer()
    end if
end sub
function onKeyEvent(key as String, press as Boolean) as Boolean
    ' since m.video is a child of `player` in the scene graph, 
    ' pressing the Back button during play will "bubble up" for us to handle here
    print "Player: keyevent = "; key

    if press and key = "back" then
        'handle Back button, by exiting play
        exitPlayer()
        return true
    end if
    return false
end function

